﻿using Microsoft.Owin;
using Owin;
using Stripe;
using WebApplication2.Models;

[assembly: OwinStartupAttribute(typeof(WebApplication2.Startup))]
namespace WebApplication2
{
    public partial class Startup
    {
        private string secretKey = "sk_test_3CLGut8U5OTOHlZqxXm1q618";
        private string publishedKey = "pk_test_Qa9P6sFaNLywMh8qpahQkOyT";
        public void Configuration(IAppBuilder app)
        {
            StripeConfiguration.SetApiKey(secretKey);
            ConfigureAuth(app);
        }
    }
}
