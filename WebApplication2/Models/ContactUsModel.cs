﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class ContactUsModel
    {
        [Key]
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime SheduledDateTime { get; set; }
        public string Details { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}