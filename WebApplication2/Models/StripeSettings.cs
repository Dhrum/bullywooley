﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class StripeSettings
    {
        public string SecretKey { get; set; }
        public string PublishedKey { get; set; }
    }
}