﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class ReferredUsers
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string OwnReferralCode { get; set; }
        public string ReferredBy { get; set; }
    }

    public class ReferredUsersList
    {
        public ReferredUsersList()
        {
            ReferredUsers = new List<ReferredUsers>();
            NonReferredUsers = new List<ReferredUsers>();
        }
        public List<ReferredUsers> ReferredUsers { get; set; }
        public List<ReferredUsers> NonReferredUsers { get; set; }
    }
}