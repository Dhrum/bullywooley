﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Product
    {
        [Key]
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string ProductImage { get; set; }
        public string ProductPath { get; set; }
        public Decimal ProductPrice { get; set; }
        public string ProductPreviewLink { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
    }

    public class Purchase
    {
        [Key]
        public string Id { get; set; }
        public string ProductId { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal PromotionalPrice { get; set; }
        public decimal Discount { get; set; }
        public int Validity { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}