﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class CheckoutFromCartModels
    {
        public CheckoutFromCartModels()
        {
            CartItems = new List<CartItemModels>();
        }
        public List<CartItemModels> CartItems { get; set; }
        public decimal TotalPrice { get; set; }
        public string Items { get; set; }
    }
}