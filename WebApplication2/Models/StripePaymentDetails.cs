﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class StripePaymentDetails
    {
        public string PublishedKey { get; set; }
        public string Desc { get; set; }
        public long?  Amount { get; set; }
        public string ProductId { get; set; }
    }
}