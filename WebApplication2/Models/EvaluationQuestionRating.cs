﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class EvaluationQuestionRating
    {
        [Key]
        public string Id { get; set; }
        public string UserId { get; set; }
        public int PartNo { get; set; }
        public int QuestionNo { get; set; }
        public int GivenRating { get; set; }
    }


}