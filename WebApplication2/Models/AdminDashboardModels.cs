﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class AdminDashboardModels
    {
        public AdminDashboardModels()
        {
            FirstWeek = new WeeklyData();
            SecondtWeek = new WeeklyData();
            ThirdWeek = new WeeklyData();
            FourthWeek = new WeeklyData();
        }
        public Int64 SiteVisitorToday { get; set; }
        public Int64 SiteVisitorLastMonth { get; set; }
        public Int64 TotalPurchases { get; set; }
        public Int64 TotalRegistered { get; set; }
        public decimal TotalSale { get; set; }
        public decimal TotalSaleLastMonth { get; set; }
        public WeeklyData FirstWeek { get; set; }
        public WeeklyData SecondtWeek { get; set; }
        public WeeklyData ThirdWeek { get; set; }
        public WeeklyData FourthWeek { get; set; }
    }

    public class WeeklyData
    {
        public Int64 Visitor { get; set; }
        public Int64 Purchases { get; set; }
        public Int64 Registration { get; set; }
        public Decimal SaleAmount { get; set; }
    }
}