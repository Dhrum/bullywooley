﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class UserProfile
    {
        [Key]
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Profession { get; set; }
        public string LicenseNumber { get; set; }
        public string MyReferralCode { get; set; }
        public string ReferredBy { get; set; }

        public string UserProfilePic { get; set; }
        public string Desc { get; set; }
    }
}