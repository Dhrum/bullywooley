﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class TakeAppointMentModel
    {
        [Key]
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime SheduledDateTime { get; set; }
        public int NoOfEnticipatedAttendees { get; set; }
        public string TimeCommitMent { get; set; }
        public string SpeakingRequestOn { get; set; }
        public string Details { get; set; }

        public DateTime CreatedOn { get; set; }


    }
}