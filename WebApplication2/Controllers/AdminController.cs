﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        ApplicationDbContext dbContext = new ApplicationDbContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            AdminDashboardModels adminDashboardModels = new AdminDashboardModels();

            return View (adminDashboardModels);
        }

        public ActionResult Sales()
        {
            var purchases = dbContext.Purchases.Select(m => m).ToList();
            return View(purchases);
        }


        public ActionResult Users()
        {
            var userList = dbContext.UserProfiles.Select(m => m).ToList();

            return View(userList);
        }

        public ActionResult Referrals()
        {
            var userList = dbContext.UserProfiles.Select(m => m).ToList();
            var referred = userList.Where(m => m.ReferredBy != "" || m.ReferredBy != null);
            var notReferred = userList.Where(m => m.ReferredBy == "" || m.ReferredBy == null);
            ReferredUsersList referredUsersList = new ReferredUsersList();
            ReferredUsers referredUsers = new ReferredUsers();
            foreach (var temp in referred)
            {
                referredUsers = new ReferredUsers();
                referredUsers.UserId = temp.Id;
                referredUsers.UserName = string.IsNullOrEmpty(temp.FullName) ? temp.FirstName + " " + temp.LastName : temp.FullName;
                referredUsers.ReferredBy = temp.ReferredBy;
                referredUsers.OwnReferralCode = temp.MyReferralCode;
                referredUsersList.ReferredUsers.Add(referredUsers);
            }

            foreach (var temp in notReferred)
            {
                referredUsers = new ReferredUsers();
                referredUsers.UserId = temp.Id;
                referredUsers.UserName = string.IsNullOrEmpty(temp.FullName) ? temp.FirstName + " " + temp.LastName : temp.FullName;
                referredUsers.ReferredBy = temp.ReferredBy;
                referredUsers.OwnReferralCode = temp.MyReferralCode;
                referredUsersList.NonReferredUsers.Add(referredUsers);
            }
            return View(referredUsersList);
        }

    }
}