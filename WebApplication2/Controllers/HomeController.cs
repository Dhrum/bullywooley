﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View ();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View ();
        }

        

        public ActionResult Slider()
        {
            return View();
        }
        public ActionResult EatingDisorderInfo()
        {
            ViewBag.Message = "Eating Disorder Infoes.";

            return View();
        }

        public ActionResult Contact()
        {
            ContactUsModel contactUsModel = new ContactUsModel();
            ViewBag.Message = "Your contact page.";

            return View(contactUsModel);
        }

        [HttpPost]
        public ActionResult Contact(ContactUsModel contactUsModel)
        {
            
            ViewBag.Message = "Your contact page.";
            contactUsModel.CreatedOn = DateTime.UtcNow;
            ViewBag.Message = "Take Appointment of Lana Grinev";
            string mailBody = "Hello Lana, <br />";
            mailBody += "Someone contaced to have an appointment of you. Here is his/her details : <br />";
            mailBody += "Name : " + contactUsModel.Name + "<br />";
            mailBody += "Email : " + contactUsModel.Email + "<br />";
            mailBody += "Scheduled Time : " + contactUsModel.SheduledDateTime.ToString() + "<br />";
            mailBody += "Details : " + contactUsModel.Details + "<br /><br />";

            mailBody += "Thank You <br /> *This email is auto generated at " + contactUsModel.CreatedOn.ToString();
            MailService ms = new MailService();
            var res = ms.SendEmail("lanagrinevlcswr@gmail.com", "Some want to contact Wellness Today 180", mailBody);


            return View(contactUsModel);
        }

        public ActionResult TakeAppointment()
        {
            TakeAppointMentModel takeAppointMentModel = new TakeAppointMentModel();
            ViewBag.Message = "Take Appointment of Lana Grinev";

            return View(takeAppointMentModel);
        }

        [HttpPost]
        public ActionResult TakeAppointment(TakeAppointMentModel takeAppointMentModel)
        {
            takeAppointMentModel.CreatedOn = DateTime.UtcNow;
            ViewBag.Message = "Take Appointment of Lana Grinev";
            string mailBody = "Hello Lana, <br />";
            mailBody += "Someone contaced to have an appointment of you. Here is his/her details : <br />";
            mailBody += "Name : " + takeAppointMentModel.Name + "<br />";
            mailBody += "Email : " + takeAppointMentModel.Email + "<br />";
            mailBody += "Scheduled Time : " + takeAppointMentModel.SheduledDateTime.ToString() + "<br />";
            mailBody += "Details : " + takeAppointMentModel.Details + "<br /><br />";

            mailBody += "Thank You <br /> *This email is auto generated at " + takeAppointMentModel.CreatedOn.ToString();
            MailService ms = new MailService();
            var res = ms.SendEmail("lanagrinevlcswr@gmail.com", "Take Appointment of Lana Grinev", mailBody);
            

            return View(takeAppointMentModel);
        }

        public ActionResult FAQ()
        {
            ViewBag.Message = "Frequently Asked Questions.";

            return View ();
        }


    }
}