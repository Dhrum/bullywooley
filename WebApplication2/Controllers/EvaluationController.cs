﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class EvaluationController : Controller
    {
        private CertificationService certificationService;
        ApplicationDbContext db = new ApplicationDbContext();
        public EvaluationController()
        {
            certificationService = new CertificationService();
            
        }
        // GET: Evaluation
        public ActionResult Index()
        {

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult CertificateDataValidation()
        {
            string name = User.Identity.GetUserName();
            string id = User.Identity.GetUserId();

            var userProfile = db.UserProfiles.Where(m => m.Id == id).FirstOrDefault();


            return View(userProfile);
        }

        [Authorize]
        public FileResult Certificate()
        {
            string name = User.Identity.GetUserName();
            string id = User.Identity.GetUserId();

            var userProfile = db.UserProfiles.Where(m=>m.Id == id).FirstOrDefault();


            string outputFileFullPath = certificationService.FillForm(name, userProfile.FullName, userProfile.Profession, userProfile.LicenseNumber, DateTime.UtcNow.ToShortDateString());

            byte[] fileBytes = System.IO.File.ReadAllBytes(outputFileFullPath);
            string fileName = Path.GetFileName(outputFileFullPath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

    }
}