﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using Microsoft.AspNet.Identity;

namespace WebApplication2.Controllers
{
    public class UserController : Controller
    {
        ApplicationDbContext dbContext = new ApplicationDbContext();
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Profile()
        {
            string id = User.Identity.GetUserId();
            var userProfile = dbContext.UserProfiles.Where(m => m.Id == id).FirstOrDefault();
            
            return View (userProfile);
        }

        [HttpPost]
        public ActionResult Profile(UserProfile userProfile)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var tempUserProfile = dbContext.UserProfiles.Where(m => m.Id == userProfile.Id).FirstOrDefault();
                    tempUserProfile.Address = userProfile.Address;
                    tempUserProfile.DateOfBirth = userProfile.DateOfBirth;
                    tempUserProfile.Desc = userProfile.Desc;
                    tempUserProfile.Email = userProfile.Email;
                    tempUserProfile.FirstName = userProfile.FirstName;
                    tempUserProfile.LastName = userProfile.LastName;
                    tempUserProfile.FullName = userProfile.FirstName + " " + userProfile.LastName;
                    tempUserProfile.LicenseNumber = userProfile.LicenseNumber;
                    tempUserProfile.Profession = userProfile.Profession;
                    tempUserProfile.Mobile = userProfile.Mobile;
                    tempUserProfile.UserProfilePic = userProfile.UserProfilePic;

                    dbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                }
            }
            
            return View();
        }

        public ActionResult Dashboard()
        {
            return View ();
        }

        [Authorize]
        public ActionResult MyProducts()
        {
            var userId = User.Identity.GetUserId();
            List<Product> products = new List<Product>();
            var allProducts = dbContext.Products.Select(m => m).ToList();

            var purchasedProducts = dbContext.UserProducts.Where(m => m.UserId == userId).ToList();

            if (purchasedProducts != null && purchasedProducts.Count > 0)
            {
                foreach (var product in purchasedProducts)
                {
                    products.Add(allProducts.Where(m => m.Id == product.ProductId).FirstOrDefault());
                }
            }

            return View(products);
        }
    }
}