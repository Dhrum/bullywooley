﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class CartController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add(string id)
        {
            CartItemModels mo = new CartItemModels();
            var product = db.Products.Where(m => m.Id == id).FirstOrDefault();
            mo.ProductId = id;
            mo.ProductImage = product.ProductImage;
            mo.ProductName = product.ProductName;
            mo.ProductPrice = product.ProductPrice;
            mo.ProductDetails = product.ProductDescription;
            
            if (Session["cart"] == null)
            {
                List<CartItemModels> li = new List<CartItemModels>();
                li.Add(mo);
                Session["cart"] = li;
                ViewBag.cart = li.Count();
                Session["count"] = 1;
            }
            else
            {
                List<CartItemModels> li = (List<CartItemModels>)Session["cart"];
                if (li.Contains(mo))
                    return RedirectToAction("MyOrder", "Cart"); ;
                li.Add(mo);
                Session["cart"] = li;
                ViewBag.cart = li.Count();
                Session["count"] = Convert.ToInt32(Session["count"]) + 1;
            }

            return RedirectToAction("MyOrder", "Cart");
        }


        public ActionResult Remove(string id)
        {
            var product = db.Products.Where(m => m.Id == id).FirstOrDefault();
            List<CartItemModels> li = (List<CartItemModels>)Session["cart"];
            li.RemoveAll(x => x.ProductId == id);
            Session["cart"] = li;
            Session["count"] = li.Count();
            ViewBag.cart = li.Count();
            return RedirectToAction("MyOrder", "Cart");

        }

        public ActionResult MyOrder()
        {
            return View((List<CartItemModels>)Session["cart"]);
        }

    }
}