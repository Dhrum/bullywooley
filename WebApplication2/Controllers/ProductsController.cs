﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using WebApplication2.Services;
using Microsoft.AspNet.Identity;
using System.IO;

namespace WebApplication2.Controllers
{
    public class ProductsController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        private ProductService productService;
        private StripeSettings stripeSettings;
        private StripeService stripeService;
        public ProductsController()
        {
            productService = new ProductService();
            stripeSettings = new StripeSettings();
            
        }
        // GET: Products
        public ActionResult Index()
        {
            List<Product> products = productService.GetAllProduct();

            return View(products);
        }

        [Authorize]
        public ActionResult PurchaseCompletion(string id)
        {
            return View(productService.GetProduct(id));
        }

        [Authorize]
        public FileResult DownloadFile(string id)
        {
            string userId = User.Identity.GetUserId();
            var purchases = db.UserProducts.Where(m => m.ProductId == id && m.UserId == userId).FirstOrDefault();

            if (purchases == null)
                return null;
            var product = db.Products.Where(m => m.Id == id).FirstOrDefault();
            product.ProductPath = "";
            string fullPath = Server.MapPath("~/" + product.ProductPath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(fullPath);
            string fileName = Path.GetFileName(fullPath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [Authorize]
        [AllowAnonymous]
        public ActionResult ProductDetails(string id)
        {
            string userName = "";
            string userId = "";
            ViewBag.IsPurchased = false;
            try
            {
                userName = User.Identity.GetUserName();
                userId = User.Identity.GetUserId();                
                var purchases = db.UserProducts.Where(m => m.ProductId == id && m.UserId == userId).FirstOrDefault();

                if (purchases != null)
                    ViewBag.IsPurchased = true;
            }
            catch (Exception)
            {

                throw;
            }

            return View(productService.GetProduct(id));
        }

        [Authorize]
        [HttpGet]
        public ActionResult Payment(string id)
        {
            string userId = User.Identity.GetUserId();

            var product = productService.GetProduct(id);
            stripeService = new StripeService("usd", Convert.ToInt64(product.ProductPrice), product.ProductName);
            StripePaymentDetails stripePaymentDetails = new StripePaymentDetails();
            stripePaymentDetails.Amount = Convert.ToInt64(product.ProductPrice) * 100;
            stripePaymentDetails.Desc = product.ProductName;
            stripePaymentDetails.ProductId = id;
            stripePaymentDetails.PublishedKey = "pk_test_Qa9P6sFaNLywMh8qpahQkOyT";

            Purchase purchase = new Purchase();
            purchase.CreatedBy = User.Identity.GetUserId();
            purchase.CreatedOn = DateTime.UtcNow;
            purchase.Id = Guid.NewGuid().ToString();
            purchase.ProductId = id;
            purchase.ProductPrice = product.ProductPrice;
            purchase.Validity = 365;

            UserProducts userProducts = new UserProducts();
            userProducts.Id = Guid.NewGuid().ToString();
            userProducts.UserId = userId;
            userProducts.ProductId = product.Id;
            userProducts.CreatedOn = DateTime.UtcNow;
            userProducts.ValidForDays = 365;

            db.UserProducts.Add(userProducts);
            db.Purchases.Add(purchase);
            db.SaveChanges();

            return View(stripePaymentDetails);
        }

        [Authorize]
        public ActionResult CheckoutFromCart()
        {
            string userId = User.Identity.GetUserId();
            List<CartItemModels> list = (List<CartItemModels>)Session["cart"];
            CheckoutFromCartModels checkoutFromCartModels = new CheckoutFromCartModels();
            string itemName = "";
            decimal totalPrice = 0;
            if (list != null || list.Count() > 0)
            {
                foreach (var item in list)
                {
                    Purchase purchase = new Purchase();
                    purchase.CreatedBy = User.Identity.GetUserId();
                    purchase.CreatedOn = DateTime.UtcNow;
                    purchase.Id = Guid.NewGuid().ToString();
                    purchase.ProductId = item.ProductId;
                    purchase.ProductPrice = item.ProductPrice;
                    purchase.Validity = 365;

                    UserProducts userProducts = new UserProducts();
                    userProducts.Id = Guid.NewGuid().ToString();
                    userProducts.UserId = userId;
                    userProducts.ProductId = item.ProductId;
                    userProducts.CreatedOn = DateTime.UtcNow;
                    userProducts.ValidForDays = 365;

                    db.UserProducts.Add(userProducts);
                    db.Purchases.Add(purchase);

                    checkoutFromCartModels.CartItems.Add(item);
                    itemName += item.ProductName + ", ";
                    totalPrice += item.ProductPrice;
                }
            }
            checkoutFromCartModels.Items = itemName;
            checkoutFromCartModels.TotalPrice = totalPrice;
            return View(checkoutFromCartModels);
        }

        //[Authorize]
        //[HttpPost]
        //public ActionResult Payment(string stripeEmail, string stripeToken)
        //{
        //    stripeService.Charge(stripeEmail, stripeToken);
        //    return View();
        //}

        [Authorize]
        public ActionResult BuyProduct(string id)
        {
            return View(productService.GetProduct(id));
        }

        public ActionResult EatingDisorder()
        {

            return View();
        }

        public ActionResult Webinar()
        {
            return View();
        }
    }
}