﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public class PurchaseService
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public PurchaseService()
        {

        }

        public void PurchaseProduct(string productId)
        {
            var product = db.Products.Where(m => m.Id == productId).FirstOrDefault();
            Purchase purchase = new Purchase();
            purchase.CreatedOn = DateTime.Now;
            purchase.ProductPrice = product.ProductPrice;
            purchase.Validity = 1;

            db.Purchases.Add(purchase);
            db.SaveChanges();
        
        }

        public List<Purchase> GetAllPurchases()
        {
            return db.Purchases.Select(m => m).ToList();
        }
    }
}