﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public class ReferralService
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public ReferralService()
        {

        }

        public bool CheckReferralCodeIsValid(string referredBy)
        {
            var listUserReferrals = db.UserReferrals.Select(m => m);
            var res = listUserReferrals.Where(m => m.MyReferralCode == referredBy).ToList();

            if (res != null && res.Count > 0)
                return true;
            else
                return false;
        }

        public string CreateUserReferral(string userId, string referredBy)
        {
            string myReferralCode = GenerateReferralCode();
            var lstReferralCodes = db.UserReferrals.Select(m => m.MyReferralCode).ToList();
            if (lstReferralCodes.Where(m => m == myReferralCode).ToList().Count > 0)
                GenerateReferralCode();

            UserReferral userReferral = new UserReferral() {
                Id = Guid.NewGuid().ToString(),
                CreatedOn = DateTime.Now,
                 MyReferralCode = myReferralCode,
                 ReferredBy = referredBy,
                 UserId = userId
            };

            try
            {
                db.UserReferrals.Add(userReferral);
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                
            }

            return myReferralCode;
        }

        private string GenerateReferralCode()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            

            return result;
        }

        public List<UserReferral> GetUserReferralsByUser(string userId)
        {

            return new List<UserReferral>();
        }
    }
}