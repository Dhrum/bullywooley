﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;

namespace WebApplication2.Services
{
    public class CertificationService
    {
        public CertificationService()
        {

        }

        public string FillForm(string userName, string userFullName, string licProf, string licNo, string dOI)
        {
            string pdfTemplate = HttpContext.Current.Server.MapPath(@"~/Certificates/CertificateTemplate.pdf");
            string newFile = HttpContext.Current.Server.MapPath(@"~/Certificates\Certificate_WT180_" + userName +".pdf");
            PdfReader pdfReader = new PdfReader(pdfTemplate);
            PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
            AcroFields pdfFormFields = pdfStamper.AcroFields;
            pdfFormFields.SetField("Name", userFullName);
            pdfFormFields.SetField("LicProf", licProf);
            pdfFormFields.SetField("LicNo", licNo);
            pdfFormFields.SetField("DOI", dOI);
            //string sTmp = "W-4 Completed for " + pdfFormFields.GetField("f1_09(0)") + " " + pdfFormFields.GetField("f1_10(0)");
            //MessageBox.Show(sTmp, "Finished");
            // flatten the form to remove editting options, set it to false  
            // to leave the form open to subsequent manual edits  
            pdfStamper.FormFlattening = false;
            // close the pdf  
            pdfStamper.Close();

            return newFile;
        }
    }
}