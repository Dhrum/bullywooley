﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Stripe;

namespace WebApplication2.Services
{
    public class StripeService
    {
        private string currency = "usd";
        private long? amount = 0;
        private string description = "test description";

        public StripeService(string currency, long? amount, string description)
        {
            this.currency = currency;
            this.amount = amount;
            this.description = description;
        }

        public void Charge(string stripeEmail, string stripeToken)
        {
            var customerService = new CustomerService();
            var chargeService = new ChargeService();

            var customer = customerService.Create(new CustomerCreateOptions { Email = stripeEmail, SourceToken = stripeToken });


            var charge = chargeService.Create(new ChargeCreateOptions {
                Amount = this.amount,
                Description = this.description ,
                Currency = this.currency,
                CustomerId = customer.Id  });
        }

    }
}