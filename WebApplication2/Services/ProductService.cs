﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public class ProductService
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public ProductService()
        {

        }

        public void CreateProduct(Product product)
        {
            db.Products.Add(product);
            db.SaveChanges();
        }

        public void UpdateProduct(Product product)
        {

        }

        public List<Product> GetAllProduct()
        {
            return db.Products.Where(m => m.IsActive).ToList();
        }

        public Product GetProduct(string productId)
        {
            return db.Products.Where(m => m.IsActive && m.Id == productId).FirstOrDefault();
        }
    }
}